# Integrador Machine Learning
Repositorio para los archivos de configuración del modelo de inteligencia artificial
basado en [Faster R-CNN + Inceptio ResNet V2](https://tfhub.dev/tensorflow/faster_rcnn/inception_resnet_v2_1024x1024/1)
## Integrantes
[Diego Ignacio Muñoz Viveros](http://gitlab.com/awerito)

[Sebastian Patricio Vidal Oñate](http://github.com/Ezfeik)
